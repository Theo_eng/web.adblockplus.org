title=Software Engineer for Adblock Plus (m/f/d)
template=job-ad
robots=none
date=Posted on September 22nd 2021
tags=<span class="tag-1">Vanilla JS</span> <span class="tag-2">CSS</span> <span class="tag-3">HTML</span>
style=pages/careers.css


## We’re hiring a Software Engineer

We are a team that serves tens of millions of people worldwide. Our vision is to provide our users with simple, intuitive tools to block distractions, protect their privacy, and put people in control of their internet experience.

Our products have been downloaded hundreds of million times and work in all major web browsers, as well as on macOS, iOS, Windows, and Android devices.
 
Our team has been fully remote since its inception, and we each work from whatever location works best for us. What unites us is a desire to give users control over their web experience.

### The role

To achieve our mission, and serve our users, we need an engineer who can help us create high-quality browser extensions to help our users navigate the web safely. You'll be joining a small, collaborative team and you'll have the opportunity to impact the browsing experience of millions of people every day.

Ultimately, success in this role will be measured by shipping products and features in our extensions that have a demonstrable positive impact on the browsing experience of our users. You'll collaborate closely with other engineers, product managers and designers, QA engineers, our user support agents and our localization experts.

We operate as a globally distributed team that spans many time zones. In this role you’ll be supporting a team based mainly in Europe and collaborate closely with colleagues in North America, so ideally you'll be able to collaborate with your peers between 3pm and 7pm CEST on an almost daily basis.

### You have

* Thorough understanding of vanilla Javascript; advanced grasp of HTML/CSS
* Knowledge about cross-browser compatibility (e.g. Chrome, Firefox, Microsoft Edge, Opera)
* You’re a team player who can explain complex concepts to technical and non-technical team members and end users
* Excellent written, verbal, and interpersonal communication skills; you enjoy collaborating with cross-functional teams

### Bonus points for

* A good understanding of open-source software and communities; we believe in open source and love working in the open source community
* Prior extension development experience
* Experience with modern javascript practices (for example React.js and other modern  libraries or frameworks) helps
* Experience with localization; we do business all over the world and our products are localized in dozens of languages
* Experience with web content accessibility

### What you’ll do

* Develop new features and enhancements for Adblock Plus, closely collaborate with other product teams, and create other extensions to help users blocks ads and other distractions in the future
* Collaborate closely with other engineers, product managers and designers,  QA engineers, our user support agents and our localization experts to deliver high-quality software to our users
* Implement dynamic, accessible UIs using modern web technologies
* Bounce ideas within the team, making architectural changes
* Handle the integration with the ad blocking code and other backend systems

### Why you’ll love our team

* **Competitive Salary:** Our salaries are based on Radford data, a widely-used global compensation benchmark, to ensure we provide competitive pay. We don’t adjust your salary based on where you live, so you can live wherever you like.
* **Comprehensive Benefits:** We offer a benefits package that includes health insurance, retirement benefits, plenty of paid vacation, paid parental leave and a monthly child care stipend. The details vary based on where you live, in the US this includes health insurance coverance and 401k matching.
* **Professional Growth:** We give team members the autonomy to do their best work. Because we’re a small team, you’ll be able to immediately see the impact of your work and grow with the team. We also support professional development with training, coaching, and regular feedback.
* **Fully Distributed Community:** You’ll be able to work 100% remotely, yet remain well-connected to your colleagues. When we’re not in the midst of a pandemic, we meet at least once a year for a week-long offsite. 
* **Generous Vacation Policy:** We encourage our employees to take the time they need for vacation, to spend time with their families, and to stay healthy by offering at least 28 days of paid leave.
* **Office Equipment:** We’ll provide you with a setup of your choice, based on what you need to work effectively.

### We'd love to work with you...

We’re serious about our work but we don’t take ourselves too seriously. We want our team to be a place where people love their work, like their co-workers, and treat everyone with respect and empathy.

We’re a knit-tight team and our strength comes from our diversity. We strive to create an inclusive environment where differences in race, sexual orientation, gender identity or expression, political and religious affiliation, socioeconomic background, cultural background, geographic location, disabilities and abilities, relationship status, veteran status, and age only make us stronger.

## How to apply

Does this role sound like a good fit? Email us at [careers@adblockplus.org](mailto:careers@adblockplus.org):

* Use `Software Engineer for Adblock Plus application` in your subject line.
* Include your resume and, optionally, a cover letter.
* Send along links that best showcase the relevant things you've built and done.

<p class="info privacy-disclaimer">Data policy: Your information will be stored in order to process your application and to communicate with you during the hiring process. <a href="privacy">Learn more in our Privacy Policy page</a>.</p>